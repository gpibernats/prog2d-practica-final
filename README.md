# Prog2d - Práctica final
Gerard Pibernat Segarra

## Cómo jugar
El juego es una copia del clásico juego *Asteroids*. El objetivo del juego es disparar y destruir asteroides evitando chocar contra los fragmentos de estos.

Para jugar se utilizan las flechas de dirección para el movimiento de la nave y barra espaciadora, o clic del ratón, para disparar.

## Escenas del proyecto

- `StartMenu`: menú principal, permite acceder al juego, a la pantalla de créditos y a la de selección de modo.
- `ClassicMode`: juego.
- `ExtremeMode`: versión del `ClassicMode`con más asteroides.

## Scripts

Los scripts están divididos en tres carpetas:

### Background

Scripts encargados de crear el efecto de fondo estrellado.

- `Star`: componente del `GameObject` `Star`. Mueve la estrella de la parte superior del viewport hasta la parte inferior.
- `StarGenerator`: genera estrellas en posiciones aleatorias.

### Menu

Controlan la navegación entre las distintas pantallas del juego.

- `MainMenu`: componente de los botones que permiten cambiar entre las pantallas de créditos, modos y el menú principal.
- `ModeMenu`: componente de los botones que permiten cambiar al modo clásico o extremo.
- `GoMainMenu`: permite volver al menú principal desde la pantalla de *game over*.

### Game

Controlan todo el *gameplay* del juego.

- `Player`: controla el comportamiento del jugador (movimiento, disparos y colisiones).
- `Bullet`: mueve en línea recta la bala disparada por el jugador.
- `Asteroid`: controla el movimiento de los asteroides, divide a los asteroides tras colisionar con una bala o los destruye si son demasiado pequeños.
- `AsteroidSpawner`: crea asteroides en posiciones aleatorias.
- `GameManager`: controla el HUD, cuenta puntos cuando el jugador destruye asteroides, descuenta vidas al jugador, termina la partida tras producirse tres muertes, muestra la pantalla de `game over`y reinicia el juego.