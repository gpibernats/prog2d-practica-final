using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public GameObject menuUI;
    public GameObject levelsUI;
    public GameObject creditsUI;
    

    void Start()
    {
        menuUI.SetActive(true);
        levelsUI.SetActive(false);
        creditsUI.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return)) {
            SceneManager.LoadScene("Level1");
        }
    }

    public void ShowMenu()
    {
        menuUI.SetActive(true);
        levelsUI.SetActive(false);
        creditsUI.SetActive(false);
    }
    
    public void ShowLevels()
    {
        menuUI.SetActive(false);
        levelsUI.SetActive(true);
        creditsUI.SetActive(false);
    }
    
    public void ShowCredits()
    {
        menuUI.SetActive(false);
        levelsUI.SetActive(false);
        creditsUI.SetActive(true);
    }
    
}
