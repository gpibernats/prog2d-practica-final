using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ModesMenu : MonoBehaviour
{
    public void ClassicMode()
    {
        SceneManager.LoadScene("ClassicMode");
    }
    
    public void ExtremeMode()
    {
        SceneManager.LoadScene("ExtremeMode");
    }
}
