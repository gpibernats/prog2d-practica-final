using UnityEngine;

public class Asteroid : MonoBehaviour
{
    public new Rigidbody2D rigidbody;
    public SpriteRenderer spriteRenderer;
    public Sprite[] sprites;

    public float size = 1f;
    public float minSize = 0.35f;
    public float maxSize = 1.65f;
    public float movementSpeed = 50.0f;
    public float maxLifetime = 30f;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        // Random properties to each asteroid
        spriteRenderer.sprite = sprites[Random.Range(0, sprites.Length)];
        transform.eulerAngles = new Vector3(0f, 0f, Random.value * 360f);

        // Set the scale and mass based on asteroid size
        transform.localScale = Vector3.one * size;
        rigidbody.mass = size;

        // Destroy the asteroid after max lifetime
        Destroy(gameObject, maxLifetime);
    }

    public void SetTrajectory(Vector2 direction)
    {
        rigidbody.AddForce(direction * movementSpeed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            // If the asteroid is large we split it in half
            if ((size * 0.5f) >= minSize)
            {
                CreateSplit();
                CreateSplit();
            }
            
            FindObjectOfType<GameManager>().AsteroidDestroyed(this);
            
            Destroy(gameObject);
        }
    }

    private Asteroid CreateSplit()
    {
        // Position
        Vector2 position = transform.position;
        position += Random.insideUnitCircle * 0.5f;

        // Asteroid half size
        Asteroid half = Instantiate(this, position, transform.rotation);
        half.size = size * 0.5f;

        // Random trajectory
        half.SetTrajectory(Random.insideUnitCircle.normalized);

        return half;
    }

}
